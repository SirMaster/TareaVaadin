package com.umg.clases;

/**
 * Created by Guicho on 13/07/2017.
 */
public class Actividades {
    String actividad;

    public Actividades() {
    }

    public Actividades(String actividad) {
        this.actividad = actividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
}
